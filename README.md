# Paint created in Assambler in Emu8086 by Carlos Carrete

This is a paint created in assembler language on EMU8086, this program as over 1800 lines of code and contains many macros for working.

## Starting 🚀

These are the things that you need for running the code, it's very simple to do.

if you have a question, please contact me on carrete.krt@gmail.com


### What do you need? 📋

You'll need only 1 program or 2 if you want to modify the code.
1.-DosBox: This program is essential if you want to run paint in assembler, since it is the one that will be running and emulating the architecture of an 8086 processor, so it is essential that you have it.
You can download it on:
```
https://www.dosbox.com/download.php?main=1
```
2.-Emu8086 (Optional): If you want to modify the code, you'll need this program, it's not necessary for running the principal code.
You can download it on:
```
https://emu8086.waxoo.com/
```

### Instalation 🔧

_It's very simple, the only thing that you need to do is download and install DosBox_
_This will get you a folder on C: called 8086, in that folder you must copy de .ams and .png file_
After that, you must open DosBox and go to the C:\8086 and execute proyecto.asm and now you can see the paint on your screen

```
proyecto.asm
```
## Version 📌

This is the last version 2.0 by  KRT

## Author ✒️

_Coder and Designer_

* **Carlos Alberto Carrete Gómez** 
